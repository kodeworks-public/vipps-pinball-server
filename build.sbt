name := "VippsServer"

version := "0.1"

scalaVersion := "2.12.5"

scalacOptions := Seq("-deprecation", "-feature", "-language:postfixOps")

libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.1.7"
libraryDependencies += "com.typesafe.akka" %% "akka-slf4j"   % "2.5.13"
libraryDependencies += "com.typesafe.akka" %% "akka-http"   % "10.1.0"
libraryDependencies += "com.typesafe.akka" %% "akka-stream" % "2.5.11"
libraryDependencies += "com.typesafe.akka" %% "akka-http-spray-json" % "10.1.1"
libraryDependencies += "io.spray" %%  "spray-json" % "1.3.3"
libraryDependencies += "com.github.andr83" %% "scalaconfig" % "0.4"