import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import server.HttpServer

import scala.concurrent.ExecutionContextExecutor

object Main extends App {
  implicit val system: ActorSystem = ActorSystem("vipps-system")
  implicit val dispatcher: ExecutionContextExecutor = system.dispatcher
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  val server = system.actorOf(HttpServer.props())
}
