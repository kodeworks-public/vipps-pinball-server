package config

object VippsSettings extends Settings {
  val username: String = configForPath("vipps.username").getString("vipps.username")
  val password: String = configForPath("vipps.password").getString("vipps.password")
  val channel: String = configForPath("vipps.channel").getString("vipps.channel")
}
