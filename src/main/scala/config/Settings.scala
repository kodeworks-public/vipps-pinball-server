package config

import com.typesafe.config.{Config, ConfigFactory}

abstract class Settings {
  val config: Config = ConfigFactory.load()
  val localConfig: Config = ConfigFactory.load("local-settings.conf")

  def configForPath(path: String): Config = {
    if (localConfig.hasPath(path))
      localConfig
    else
      config
  }
}
