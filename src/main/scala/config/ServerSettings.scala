package config

import DeviceConfigProtocol._
import com.github.andr83.scalaconfig._

object ServerSettings extends Settings {
  private val devicesPath = "server.devices"
  private val timeoutPath = "server.cancel.timeout"
  private val ipPath = "server.ip"
  private val portPath = "server.port"
  private val adminsPath = "server.admins"

  val devices: List[Device] = configForPath(devicesPath).asUnsafe[List[Device]](devicesPath)
  val itemIds: List[Int] = devices.flatMap(device => device.transactions.map(trans => trans.id))
  val timeout: Option[Int] = configForPath(timeoutPath).asUnsafe[Option[Int]](timeoutPath)
  val ip: String = configForPath(ipPath).asUnsafe[String](ipPath)
  val port: Int = configForPath(portPath).asUnsafe[Int](portPath)
  val admins: List[Int] = configForPath(adminsPath).as[List[Int]](adminsPath).getOrElse(List.empty[Int])
}
