package config

import com.typesafe.config.Config
import com.github.andr83.scalaconfig._

case class Transaction(id: Int, credits: Int, debug: Boolean = false)

case class Device(id: String, transactions: List[Transaction], name: String = "") {
  override def toString: String = if (name.nonEmpty) name else id
}

object DeviceConfigProtocol {
  implicit def TransactionReader: Reader[Transaction] = Reader.pure((config: Config, path: String) => {
    val conf = config.getConfig(path)
    val id = conf.asUnsafe[Int]("id")
    val credits = conf.asUnsafe[Int]("credits")
    val debug = conf.as[Boolean]("debug") match {
      case Right(value) => value
      case _ => false
    }

    Transaction(id, credits, debug)
  })

  implicit def DeviceReader: Reader[Device] = Reader.pure((config: Config, path: String) => {
    val conf = config.getConfig(path)
    val id = conf.asUnsafe[String]("id")
    val transactions = conf.asUnsafe[List[Transaction]]("transactions")
    conf.as[String]("name") match {
      case Right(name) => Device(id, transactions, name)
      case Left(_) => Device(id, transactions)
    }
  })
}
