package server

import java.time.LocalDateTime

import akka.actor.{Actor, ActorLogging, ActorSystem, Cancellable, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.HttpResponse
import akka.http.scaladsl.server.{Directives, Route}
import akka.stream.ActorMaterializer
import config.{Device, ServerSettings}
import vippsClient.VippsClient
import vippsOrder.OrderStatus._
import vippsOrder.{Order, OrderAction, OrderHandler}

import scala.collection.mutable
import scala.concurrent.duration._
import scala.concurrent.{ExecutionContextExecutor, Future}

sealed abstract class PendingAction
object New extends PendingAction
object Claimed extends PendingAction

object HttpServer {
  def props(): Props = Props(new HttpServer)
}

class HttpServer extends Actor with ActorLogging {
  implicit val system: ActorSystem = ActorSystem("vipps-system")
  implicit val dispatcher: ExecutionContextExecutor = system.dispatcher
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  private val vippsClient = context.actorOf(VippsClient.props())
  private var bindingFuture: Future[Http.ServerBinding] = _
  private val lastConnected = mutable.Map[String, LocalDateTime]()
  private val disconnectInterval = java.time.Duration.ofSeconds(30)
  private val disconnectCancellable = mutable.Map[String, Cancellable]()
  private val orderHandler = new OrderHandler(
    ServerSettings.timeout.getOrElse(10) seconds,
    ServerSettings.admins,
    system.scheduler
  )

  object JsonService extends Directives with DeviceRequestJsonSupport {
    val route: Route =
      path("get-transactions") {
        post {
          entity(as[TransactionRequest]) { request =>
            getDevice(request.devID) match {
              case Some(device) =>
                onDeviceConnected(device)
                complete(TransactionResponse(transactionsForDevice(device)))
              case None => complete(HttpResponse(403))
            }
          }
        }
      } ~
      path("complete-transaction") {
        post {
          entity(as[CompleteRequest]) { request =>
            getDevice(request.devID) match {
              case Some(device) =>
                onDeviceConnected(device)
                completeTransaction(device, request.transID, request.status)
                complete(HttpResponse(200))
              case None => complete(HttpResponse(403))
            }
          }
        }
      }
  }

  override def preStart(): Unit = {
    super.preStart()
    bindingFuture = Http().bindAndHandle(JsonService.route, ServerSettings.ip, ServerSettings.port)
  }

  override def postStop(): Unit = {
    bindingFuture.flatMap(_.unbind())
    super.postStop()
  }

  override def receive: Receive = {
    case order: Order => handleOrder(order)
    case msg => log.warning("Unknown message {}", msg)
  }

  private def onDeviceConnected(dev: Device): Unit = {
    lastConnected get dev.id match {
      case Some(lastTime) =>
        // If last connected time was more than 30 seconds ago
        val duration = java.time.Duration.between(lastTime, LocalDateTime.now())
        if (duration.compareTo(disconnectInterval) > 0) {
          log.info("Device {} reconnected after {}", dev, duration)
        }
      case None => log.info("Device {} connected: {}", dev, dev.id)
    }

    disconnectCancellable get dev.id match {
      case Some(c) => c.cancel()
      case None =>
    }

    lastConnected(dev.id) = LocalDateTime.now()
    disconnectCancellable(dev.id) = system.scheduler.scheduleOnce(disconnectInterval.getSeconds seconds) {
      log.info("Device {} disconnected", dev)
    }
  }

  private def getDevice(id: String): Option[Device] = ServerSettings.devices collectFirst {
    case device: Device if device.id == id => device
  }

  private def transactionsForDevice(dev: Device): List[Transaction] = {
    orderHandler.getPendingForDevice(dev) map {
      case (order, creds) =>
        order.status = Claimed
        Transaction(order.order.id, creds)
    }
  }

  private def completeTransaction(dev: Device, transID: Int, status: Int): Unit = {
    orderHandler.getPending(transID) match {
      case Some(pendingOrder) if orderHandler.orderIsValidForDevice(pendingOrder.order, dev) =>
        status match {
          case 0 =>
            log.info("Device {} claimed {} credits", dev, orderHandler.getCredits(dev, pendingOrder.order))
            vippsClient ! OrderAction(pendingOrder.order, vippsOrder.Accept)
          case 1 =>
            log.info("Device {} failed to add credits. Cancelling order", dev)
            vippsClient ! OrderAction(pendingOrder.order, vippsOrder.Cancel)
          case _ =>
            log.warning("Invalid completion status {}", status)
        }
      case _ => log.warning("No pending order with id {}", transID)
    }
  }

  private def handleOrder(order: Order) = order.status match {
    case Persist =>
      orderHandler.handleOrder(order) {
        log.info("Cancelling order {}", order.id)
        vippsClient ! OrderAction(order, vippsOrder.Cancel)
      }
    case Confirmed => vippsClient ! OrderAction(order, vippsOrder.Deliver)
    case Delivered | Cancelled => orderHandler.cancelOrder(order)
  }
}
