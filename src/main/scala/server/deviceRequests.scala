package server

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json._

case class Transaction(id: Int, credits: Int)
case class TransactionRequest(devID: String)
case class TransactionResponse(transactions: List[Transaction])
case class CompleteRequest(devID: String, transID: Int, status: Int)

trait DeviceRequestJsonSupport extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val TransactionFormat: JsonFormat[Transaction] = jsonFormat2(Transaction)
  implicit val TransactionRequestFormat: RootJsonFormat[TransactionRequest] = jsonFormat1(TransactionRequest)
  implicit val TransactionResponseFormat: RootJsonFormat[TransactionResponse] = jsonFormat1(TransactionResponse)
  implicit val CompleteRequestFormat: RootJsonFormat[CompleteRequest] = jsonFormat3(CompleteRequest)
}
