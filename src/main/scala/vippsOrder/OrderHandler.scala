package vippsOrder

import akka.actor.{Cancellable, Scheduler}
import config.{Device, ServerSettings, Transaction}
import server.{New, PendingAction}

import scala.collection.mutable
import scala.concurrent.ExecutionContext
import scala.concurrent.duration.FiniteDuration

case class PendingOrder(order: Order, var status: PendingAction, cancellable: Cancellable)

class OrderHandler(cancelTimeout: FiniteDuration, admins: List[Int], scheduler: Scheduler)(implicit ec: ExecutionContext) {
  private val pendingOrders = new mutable.ListBuffer[PendingOrder]()

  def add(pendingOrder: PendingOrder): Unit = pendingOrders += pendingOrder

  def orderIsValid(order: Order): Boolean = {
    order.itemIds.collect({
      case id: Int if ServerSettings.itemIds.contains(id) => true
    }).nonEmpty
  }

  def orderIsValidForDevice(order: Order, device: Device): Boolean = {
    ServerSettings.devices.flatMap(dev => {
      dev.transactions.map(_.id)
    }).collect({
      case id: Int if order.itemIds.contains(id) => true
    }).nonEmpty
  }

  def getPending(id: Int): Option[PendingOrder] = {
    pendingOrders collectFirst {
      case pendingOrder: PendingOrder =>
        if (pendingOrder.order.id == id) pendingOrder
    } match {
      case Some(pendingOrder: PendingOrder) => Some(pendingOrder)
      case _ => None
    }
  }

  def getPending(order: Order): Option[PendingOrder] = getPending(order.id)

  def getPendingForDevice(device: Device): List[(PendingOrder, Int)] = {
    pendingOrders.collect({
      case pendingOrder @ PendingOrder(order, status, _) if orderIsValidForDevice(order, device) && status == New =>
        (pendingOrder, getCredits(device, order))
    }).filter({
      case (_, num: Int) => num > 0
    }).toList
  }

  def getCredits(device: Device, order: Order): Int = {
    order.data.woOrder.orderLines.flatMap(orderItem => {
      device.transactions collect {
        case Transaction(transId, credits, debug) if orderItem.itemId == transId && (!debug || admins.contains(order.data.customerId)) =>
          credits * orderItem.quantity
      }
    }).sum
  }

  def handleOrder(order: Order)(onCancel: => Unit): Unit = {
    if (orderIsValid(order)) {
      val cancellable = scheduler.scheduleOnce(cancelTimeout)(onCancel)
      add(PendingOrder(order, New, cancellable))
    }
  }

  def cancelOrder(order: Order): Boolean = {
    getPending(order) match {
      case Some(pendingOrder) =>
        pendingOrder.cancellable.cancel()
        pendingOrders -= pendingOrder
        true
      case None => false
    }
  }
}
