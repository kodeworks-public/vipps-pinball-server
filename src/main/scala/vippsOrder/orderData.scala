package vippsOrder

import spray.json._

case class OrderItem(itemId: Int, quantity: Int)
case class woOrder(orderLines: List[OrderItem])
case class OrderData(id: Int, woOrder: woOrder, customerId: Int)

object OrderDataJsonProtocol extends DefaultJsonProtocol {
  implicit val OrderItemJsonFormat: JsonFormat[OrderItem] = jsonFormat2(OrderItem)
  implicit val woOrderJsonFormat: JsonFormat[woOrder] = jsonFormat1(woOrder)
  implicit val OrderDataJsonFormat: JsonFormat[OrderData] = jsonFormat3(OrderData)
}
