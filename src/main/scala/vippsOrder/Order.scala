package vippsOrder

import vippsOrder.OrderStatus.OrderStatus

object OrderStatus extends Enumeration {
  type OrderStatus = Value
  val Persist, Confirmed, Delivered, Cancelled = Value
}

class Order(val status: OrderStatus, val data: OrderData) {
  val id: Int = data.id
  val itemIds: List[Int] = data.woOrder.orderLines.map(item => item.itemId)
}

sealed abstract class Action {
  val action: String
}

object Accept extends Action { val action = "accept"}
object Deliver extends Action { val action = "deliver"}
object Cancel extends Action { val action = "reject"}

case class OrderAction(order: Order, action: Action)
