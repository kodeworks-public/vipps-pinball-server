package vippsClient

import akka.Done
import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, Cancellable, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.model.ws.{Message, TextMessage, WebSocketRequest}
import akka.stream.scaladsl.{Keep, Sink, Source}
import akka.stream.{ActorMaterializer, OverflowStrategy}
import spray.json._
import vippsClient.VippsWebsocketClient._
import vippsClient.WebsocketJsonProtocol._
import vippsOrder.OrderDataJsonProtocol._
import vippsOrder.{Order, OrderData, OrderStatus}

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContextExecutor, Future}

object VippsWebsocketClient {
  def props(): Props = Props(new VippsWebsocketClient)

  final case class ConnectWebsocket()
  final case class CloseWebsocket()
  final case class WebsocketClosed()
  final case class ConnectionEstablished(socketId: String)
  final case class SubscriptionSucceeded()
  final case class Subscribe(auth: String, channel: String)
}

class VippsWebsocketClient extends Actor with ActorLogging {
  implicit val system: ActorSystem = ActorSystem("vipps-system")
  implicit val dispatcher: ExecutionContextExecutor = system.dispatcher
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  private val url = "wss://ws-mt1.pusher.com/app/b23de9c78810b71ca94c?protocol=7&client=js&version=3.2.4&flash=false"
  private var websocket: Option[ActorRef] = None
  private var pingScheduler: Option[Cancellable] = None
  private var closing = false

  override def preStart(): Unit = {
    super.preStart()
    connect()
    pingScheduler = Some(system.scheduler.schedule(10 second, 30 second)(ping()))
  }

  override def postStop(): Unit = close(); super.postStop()

  override def receive: Receive = {
    case ConnectWebsocket() => connect()
    case CloseWebsocket() => close()
    case Subscribe(auth, channel) =>
      val msg = WebsocketMessage("pusher:subscribe", SubscribeData(auth, channel).toJson).toJson.compactPrint
      websocket.get ! TextMessage.Strict(msg)
    case msg => log.warning("Unknown message {}", msg)
  }

  private def close(): Unit = {
    if (closing) return
    closing = true

    pingScheduler match {
      case Some(cancellable) => cancellable.cancel()
      case None =>
    }

    websocket match {
      case Some(ws) => ws ! TextMessage.Strict(WebsocketMessage("pusher:close").toJson.compactPrint)
      case None =>
    }

    context.parent ! WebsocketClosed()
  }

  private def connect(): Unit = {
    log.info("Connecting websocket")
    closing = false

    val flow = Http().webSocketClientFlow(WebSocketRequest(url))

    val messageSink: Sink[Message, Future[Done]] =
      Sink.foreach {
        case message: TextMessage.Strict =>
          handleMessage(message.text)
        case message => log.info("Unknown websocket message {}", message)
      }

    val messageSource: Source[Message, ActorRef] =
      Source.actorRef[TextMessage.Strict](bufferSize = 10, OverflowStrategy.fail)

    val ((ws: ActorRef, upgradeResponse), closed) =
      messageSource
        .viaMat(flow)(Keep.both)
        .toMat(messageSink)(Keep.both)
        .run()

    websocket = Some(ws)

    upgradeResponse.map { upgrade =>
      if (upgrade.response.status == StatusCodes.SwitchingProtocols) {
        Done
      } else {
        throw new RuntimeException(s"Connection failed: ${upgrade.response.status}")
      }
    }

    closed.onComplete(_ => close())
  }

  private def ping(): Unit = {
    websocket.get ! TextMessage.Strict(WebsocketMessage("pusher:ping", JsObject.empty).toJson.compactPrint)
  }

  private def handleMessage(messageStr: String): Unit = {
    val message = messageStr.parseJson.convertTo[WebsocketMessage]

    message.event match {
      case "pusher:connection_established" =>
        val data = message.data.convertTo[ConnectionEstablishedData]
        context.parent ! ConnectionEstablished(data.socket_id)
      case "pusher_internal:subscription_succeeded" =>
        log.info("Subscription succeeded")
        context.parent ! SubscriptionSucceeded()
      case "order_persist" => context.parent ! new Order(OrderStatus.Persist, message.data.convertTo[OrderData])
      case "order_confirmed" => context.parent ! new Order(OrderStatus.Confirmed, message.data.convertTo[OrderData])
      case "order_delivered" => context.parent ! new Order(OrderStatus.Delivered, message.data.convertTo[OrderData])
      case "order_cancelled" => context.parent ! new Order(OrderStatus.Cancelled, message.data.convertTo[OrderData])
      case "pusher:pong" =>
      case _ => log.error("Unknown event '{}'", message.event)
    }
  }
}
