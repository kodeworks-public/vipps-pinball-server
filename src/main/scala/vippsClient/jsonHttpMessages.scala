package vippsClient

import spray.json._

case class WebsocketAuthData(auth: String)

object HttpJsonProtocol extends DefaultJsonProtocol {
  implicit val WebsocketAuthDataJsonFormat: RootJsonFormat[WebsocketAuthData] = jsonFormat1(WebsocketAuthData)
}
