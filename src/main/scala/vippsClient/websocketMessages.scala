package vippsClient

import spray.json._

case class WebsocketMessage(event: String, data: JsValue = JsObject.empty)

case class ConnectionEstablishedData(socket_id: String, activity_timeout: Int)

case class SubscribeData(auth: String, channel: String)

object WebsocketJsonProtocol extends DefaultJsonProtocol {
  implicit object WebsocketMessageFormat extends RootJsonFormat[WebsocketMessage] {
    def write(msg: WebsocketMessage) = JsObject("event" -> JsString(msg.event), "data" -> msg.data)

    def read(value: JsValue) = {
      value.asJsObject.getFields("event", "data") match {
        case Seq(JsString(event), JsString(data)) => WebsocketMessage(event, data.parseJson)
      }
    }
  }

  implicit val ConnectionEstablishedFormat: JsonFormat[ConnectionEstablishedData] = jsonFormat2(ConnectionEstablishedData)
  implicit val SubscribeFormat: JsonFormat[SubscribeData] = jsonFormat2(SubscribeData)
}
