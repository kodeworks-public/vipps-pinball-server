package vippsClient

import akka.actor.{Actor, ActorLogging, ActorSystem, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers._
import akka.stream.ActorMaterializer
import spray.json._
import vippsClient.HttpJsonProtocol._
import vippsClient.VippsWebClient._
import vippsOrder.OrderAction

import scala.collection.mutable
import scala.concurrent.duration._
import scala.concurrent.{ExecutionContextExecutor, Future, Promise}
import scala.util.matching.Regex
import scala.util.{Failure, Success}

object VippsWebClient {
  def props(username: String, password: String): Props = Props(new VippsWebClient(username, password))

  final case class LoginSuccess()
  final case class WebsocketAuth(socketId: String, channel: String)
}

class VippsWebClient(
                      private val username: String,
                      private val password: String
                    ) extends Actor with ActorLogging {
  implicit val system: ActorSystem = ActorSystem("vipps-system")
  implicit val dispatcher: ExecutionContextExecutor = system.dispatcher
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  val loginPageUrl: Uri = "https://vippsadmin.snappo.com/login"
  val loginCheckUrl: Uri = "https://vippsadmin.snappo.com/login_check"
  val websocketAuthUrl: Uri = "https://vippsadmin.snappo.com/pusher/auth"
  val loginSuccessUrl: Uri = "/secure/snappaway/?loginSuccess=true"
  val serverCheckUrl: Uri = "https://vippsadmin.snappo.com/check-server.php"
  val orderActionUrl: Uri = "https://vippsadmin.snappo.com/secure/snappaway/order/"

  val loginPageCSRFRegex: Regex = "\\\"_csrf_token\\\" value=\\\"(.*?)\\\"".r
  private val cookieMap = mutable.Map[String, String]()

  override def preStart(): Unit = {
    super.preStart()
    login().onComplete(_ => {
      context.parent ! LoginSuccess()
    })
    system.scheduler.schedule(30 seconds, 30 seconds)(checkServer())
  }

  override def receive: Receive = {
    case WebsocketAuth(socketId, channel) => websocketAuth(socketId, channel)
    case orderAction: OrderAction => handleOrder(orderAction)
    case msg => log.warning("Unknown message {}", msg)
  }

  private def setCookies(response: HttpResponse): Unit = {
    response.headers.foreach {
      case c: `Set-Cookie` => cookieMap(c.cookie.name) = c.cookie.value
      case _ =>
    }
  }

  private def cookies: List[Cookie] = {
    if (cookieMap.nonEmpty) {
      List(Cookie(cookieMap.map {
        case (name, value) => HttpCookiePair(name, value)
      }.toList))
    } else {
      List()
    }
  }

  private def getCSRFToken(html: String): Option[String] = {
    loginPageCSRFRegex.findFirstMatchIn(html) match {
      case Some(m) => Some(m.group(1))
      case None => None
    }
  }

  private def login(): Future[Unit] = {
    val loginPromise = Promise[Unit]

    val responseFuture = Http().singleRequest(HttpRequest(uri = loginPageUrl))
    responseFuture.onComplete {
      case Success(res) =>
        setCookies(res)
        val bs = res.entity.toStrict(3.seconds).map { _.data }
        val htmlFuture = bs.map(_.utf8String)

        htmlFuture.onComplete({
          case Success(html) => getCSRFToken(html) match {
              case Some(csrf) => loginCheck(csrf, loginPromise)
              case None => log.error("Couldn't find the CSRF token")
          }
          case Failure(cause) => throw cause
        })
      case Failure(cause) => throw cause
    }

    loginPromise.future
  }

  private def loginCheck(csrf: String, loginPromise: Promise[Unit]): Unit = {
    val data = FormData(Map("_username" -> username, "_password" -> password, "_csrf_token" -> csrf))
    val request = HttpRequest(
      HttpMethods.POST,
      loginCheckUrl,
      cookies,
      data.toEntity
    )
    val loginResponseFuture = Http().singleRequest(request)

    loginResponseFuture.onComplete {
      case Success(res) =>
        val location = res.headers[Location]
        if (location.contains(Location(loginSuccessUrl))) {
          setCookies(res)
          loginPromise success Unit
          res.discardEntityBytes()
        } else {
          log.error("Couldn't log in. Response:\n{}", res)
          res.discardEntityBytes()
          throw new Exception("Couldn't log in")
        }
      case Failure(cause) => throw cause
    }
  }

  private def runRequestLoggedIn(request: HttpRequest, numTries: Int = 0): Future[HttpResponse] = {
    val promise = Promise[HttpResponse]()

    Http().singleRequest(request).onComplete({
      case Success(res) =>
        if (res.status != StatusCodes.OK) {
          if (numTries < 5) {
            log.error("Vipps returned not OK")
            log.info("Trying to log in again")
            login().onComplete(_ => {
              log.info("Logged in. Retrying order action")
              runRequestLoggedIn(request, numTries + 1) onComplete { res => promise complete res }
            })
          } else {
            log.error("Login failed 5 times in a row")
            promise failure new RuntimeException("Can't login to Vipps server")
          }

          res.discardEntityBytes()
        } else {
          promise success res
        }
      case Failure(cause) => promise failure cause
    })

    promise.future
  }

  private def getResponseData(response: HttpResponse): Future[String] = {
    val promise = Promise[String]()
    val bs = response.entity.toStrict(3.seconds).map { _.data }
    val dataFuture = bs.map(_.utf8String)

    dataFuture.onComplete(result => {
      promise complete result
      response.discardEntityBytes()
    })

    promise.future
  }

  private def checkServer(): Unit = {
    val request = HttpRequest(uri = serverCheckUrl, headers = cookies)
    Http().singleRequest(request).onComplete {
      case Success(res) =>
        setCookies(res)
        res.discardEntityBytes()
      case Failure(cause) => throw cause
    }
  }

  private def handleOrder(orderAction: OrderAction, numTries: Int = 0): Unit = {
    val url = orderActionUrl + orderAction.order.id.toString + "/" + orderAction.action.action
    val request = HttpRequest(uri = url, headers = cookies)
    runRequestLoggedIn(request) onComplete {
      case Success(res) => res.discardEntityBytes()
      case Failure(cause) => throw cause
    }
  }

  private def websocketAuth(socketId: String, channel: String): Unit = {
    val data = FormData(Map("socket_id" -> socketId, "channel_name" -> channel))
    val request = HttpRequest(
      HttpMethods.POST,
      websocketAuthUrl,
      cookies,
      data.toEntity
    )

    runRequestLoggedIn(request).onComplete {
      case Success(res) =>
        getResponseData(res).onComplete({
          case Success(html) =>
            context.parent ! html.parseJson.convertTo[WebsocketAuthData]
          case Failure(cause) => throw cause
        })
      case Failure(cause) => throw cause
    }
  }
}
