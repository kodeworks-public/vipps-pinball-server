package vippsClient

import akka.actor.{Actor, ActorLogging, ActorSystem, Props}
import akka.stream.ActorMaterializer
import config.VippsSettings
import vippsClient.VippsWebClient._
import vippsClient.VippsWebsocketClient._
import vippsOrder.{Order, OrderAction}

import scala.concurrent.ExecutionContextExecutor

object VippsClient {
  def props(): Props = Props(new VippsClient)
}

class VippsClient extends Actor with ActorLogging {
  implicit val system: ActorSystem = ActorSystem("vipps-system")
  implicit val dispatcher: ExecutionContextExecutor = system.dispatcher
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  private val username = VippsSettings.username
  private val password = VippsSettings.password
  private val websocketChannel = VippsSettings.channel
  private val websocketClient = context.actorOf(VippsWebsocketClient.props())
  private val webClient = context.actorOf(VippsWebClient.props(username, password))
  private var loggedIn = false
  private var websocketEstablished = false
  private var websocketId: String = _

  override def receive: Receive = {
    case LoginSuccess() =>
      loggedIn = true
      log.info("Logged in")
      if (loggedIn && websocketEstablished) websocketAuth()
    case ConnectionEstablished(socketId) =>
      websocketEstablished = true
      websocketId = socketId
      if (loggedIn && websocketEstablished) websocketAuth()
    case WebsocketAuthData(auth) => websocketClient ! Subscribe(auth, websocketChannel)
    case _: SubscriptionSucceeded =>
    case WebsocketClosed() => reconnect()
    case order: Order => context.parent ! order
    case orderAction: OrderAction => webClient forward orderAction
    case msg => log.warning("Unknown message {}", msg)
  }

  private def websocketAuth(): Unit = {
    webClient ! WebsocketAuth(websocketId, websocketChannel)
  }

  private def reconnect(): Unit = {
    websocketEstablished = false
    websocketClient ! ConnectWebsocket()
  }
}
